1

SELECT customerName FROM customers WHERE country = "Philippines";

2

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

3

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

4

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

5

SELECT customerName FROM customers WHERE state IS NULL;

6

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

7

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

8

SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

9

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

10

SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

11

SELECT DISTINCT country FROM customers;

12

SELECT DISTINCT status FROM orders;

13

SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

14

SELECT employees.firstName, employees.lastName, offices.city
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE city = "Tokyo";

15

SELECT customerName
FROM customers 
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE firstName = "Leslie" AND lastName = "Thompson";

16

SELECT products.productName, customers.customerName
FROM products
JOIN orderdetails ON orderdetails.productCode = products.productCode
JOIN orders ON orders.orderNumber = orderdetails.orderNumber
JOIN customers ON customers.customerNumber = orders.customerNumber
WHERE customers.customerName = "Baane Mini Imports"


17

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE customers.country = offices.country;

18

SELECT s1.lastName, s1.firstName
FROM employees s1 
INNER JOIN employees s2
ON s1.reportsTo = s2.employeeNumber
WHERE  s2.lastName = "Bow" AND s2.firstName = "Anthony";

19

SELECT productName, MAX(MSRP) FROM products;

20

SELECT COUNT(*) FROM customers WHERE country = "UK";

21

SELECT COUNT(*) FROM products GROUP BY productLine;

22

SELECT COUNT(*) FROM customers GROUP BY salesRepEmployeeNumber;

23 

SELECT productName, quantityInStock FROM products WHERE productLine LIKE "%planes%" AND quantityInStock < 1000;



